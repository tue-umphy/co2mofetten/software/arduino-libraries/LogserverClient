# LogserverClient Arduino library

This library provides a class that extends network clients (e.g. `GSMClient`,
`EthernetClient`, or `WifiClient`) with utility methods simplifying the upload
to a
[python3-co2logserver](https://gitlab.com/tue-umphy/co2mofetten/python3-co2logserver).

## Capabilities

- Data upload in JSON or [MsgPack](https://msgpack.org) format
- Upload authentication by [payload salting](https://gitlab.com/tue-umphy/co2mofetten/python3-co2logserver#authentication)
- Parsing date and time from HTTP response header

## Requirements

- [Arduino-MD5 library](https://github.com/tzikis/ArduinoMD5/)
- [ArduinoJson library](https://arduinojson.org) (v5 and v6 are supported)
- [Time library](https://github.com/PaulStoffregen/Time)

## Example

There are examples for the network clients `GSMClient`, `EthernetClient` and
`WifiClient` under the `examples` folder.

The basic usage is the same:

```c++
#include <LogserverClient.h>
#include <ArduinoJson.h>

// instantiate a LogserverClient object
// CLIENTTYPE can be GSMClient, WiFiClient, EthernetClient, etc...
LogserverClient<CLIENTTYPE> client;
// The `client` object now behaves exactly the same as an object of the
// CLIENTTYPE plus some utility methods

// Uploading an ArduinoJson::JsonObject is as easy as:
LOGSERVER_UPLOAD_STATUS uploadstatus = client.upload(
  uploadjson, // ArduinoJson::JsonObject data
  DATA_SERVER_PORT, // port
  LOGSERVER_UPLOAD_FORMAT::JSON, // JSON or MSGPACK format
  DATA_SERVER_SALT, // salt
  4, // salt size
  3, // connection attempts
  20e3 // response timeout ms
  );
```

## Debugging

You can get the library to talk about every action it takes by setting the
compiler flag `LSC_DEBUG` (e.g. set `build_flags = -DLSC_DEBUG` if you are
using PlatformIO, I don't know how to do this in the Arduino IDE directly, but
you can always just do a `#define LSC_DEBUG` in the `LogserverClient.h`
header file to accomplish this.)
