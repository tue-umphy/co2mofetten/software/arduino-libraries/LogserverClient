/*

# Uploading data to a co2logserver via Ethernet using the LogserverClient lib.

## Hardware

- Arduino Mega
- Ethernet-Shield

## Libraries

- Ethernet
- SPI
- LogserverClient
- ArduinoMD5
- ArduinoJson (both v5 and v6 should work)

## Server

- python3-co2logserver
  - https://gitlab.com/tue-umphy/co2mofetten/python3-co2logserver
  - settings according to **co2logserver settings** below
  - configured to accept a salt as given below

*/

#include <ArduinoJson.h>
#include <Ethernet.h>
#include <LogserverClient.h>

// MAC-address of the Ethernet-Shield
byte mac[] = { 0x8a, 0xA5, 0xD6, 0x13, 0x79, 0xE6 };
IPAddress ip(192, 168, 0, 177);
IPAddress myDns(192, 168, 0, 1);

// co2logserver settings
#define DATA_SERVER "CO2LOGSERVER-IP-OR-DOMAIN"
#define DATA_SERVER_PATH "/upload"
#define DATA_SERVER_PORT 8080
#define DATA_SERVER_SALT "ENTER-YOUR-SALT-HERE"

#define DEBUGS(x)                                                             \
  Serial.println(F(x));                                                       \
  Serial.flush();
#define DEBUGSN(x, y)                                                         \
  Serial.print(F(x));                                                         \
  Serial.println(y);                                                          \
  Serial.flush();
#define DEBUGSNS(x, y, z)                                                     \
  Serial.print(F(x));                                                         \
  Serial.print(y);                                                            \
  Serial.println(F(z));                                                       \
  Serial.flush();

unsigned int bme280_pressure_hPa;
unsigned short bme280_humidity_percent;
unsigned int scd30_co2_ppm;
float scd30_temperature_c;

LogserverClient<EthernetClient> client;

// For some reason, checking the ARDUINOJSON_VERSION_MAJOR is not set on every
// compilation run that (PlatformIO?) is performed. This might have to do with
// some preprocessing that is performed on *.ino-files before CPP-compilation.
// This means we have to define two differently named functions here to prevent
// compiler errors...
#if ARDUINOJSON_VERSION_MAJOR >= 6
JsonObject
createjson_v6()
{
  const int uploadjsoncapacity = 4 * JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(4);
  StaticJsonDocument<uploadjsoncapacity> uploadjsondoc;
  JsonObject uploadjson = uploadjsondoc.to<JsonObject>();
  JsonArray humidity_array =
    uploadjson.createNestedArray("bme280_humidity_percent");
  humidity_array.add(bme280_humidity_percent);
  JsonArray pressure_array =
    uploadjson.createNestedArray("bme280_pressure_hPa");
  pressure_array.add(bme280_pressure_hPa);
  JsonArray temperature_array =
    uploadjson.createNestedArray("scd30_temperature_c");
  temperature_array.add(scd30_temperature_c);
  JsonArray co2_array = uploadjson.createNestedArray("scd30_co2_ppm");
  co2_array.add(scd30_co2_ppm);
  return uploadjson;
}
#else
JsonObject&
createjson_v5()
{
  const int uploadjsoncapacity = 4 * JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(4);
  StaticJsonBuffer<uploadjsoncapacity> uploadjsonbuffer;
  JsonObject& uploadjson = uploadjsonbuffer.createObject();
  JsonArray& humidity_array =
    uploadjson.createNestedArray("bme280_humidity_percent");
  humidity_array.add(bme280_humidity_percent);
  JsonArray& pressure_array =
    uploadjson.createNestedArray("bme280_pressure_hPa");
  pressure_array.add(bme280_pressure_hPa);
  JsonArray& temperature_array =
    uploadjson.createNestedArray("scd30_temperature_c");
  temperature_array.add(scd30_temperature_c);
  JsonArray& co2_array = uploadjson.createNestedArray("scd30_co2_ppm");
  co2_array.add(scd30_co2_ppm);
  return uploadjson;
}
#endif

void
setup()
{
  Serial.begin(115200);
  randomSeed(analogRead(0));
  // start the Ethernet connection:
  DEBUGS("Initialize Ethernet with DHCP:");
  if (not Ethernet.begin(mac)) {
    DEBUGS("Failed to configure Ethernet using DHCP");
    // Check for Ethernet hardware present
    if (Ethernet.hardwareStatus() == EthernetNoHardware) {
      DEBUGS("Ethernet shield not found.");
      while (true)
        ;
    }
    if (Ethernet.linkStatus() == LinkOFF) {
      DEBUGS("Ethernet cable is not connected.");
    }
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip, myDns);
  } else {
    DEBUGSN("  DHCP assigned IP ", Ethernet.localIP());
  }
  // give the Ethernet shield a second to initialize:
  delay(1000);
}

void
measurement()
{
  bme280_humidity_percent = random(0, 100);
  DEBUGSNS("         humidity: ", bme280_humidity_percent, " %");
  bme280_pressure_hPa = random(900, 1100);
  DEBUGSNS("         pressure: ", bme280_pressure_hPa, " hPa");
  scd30_temperature_c = (float)random(100, 400) / 10;
  DEBUGSNS("      temperature: ", scd30_temperature_c, " °C");
  scd30_co2_ppm = random(10000);
  DEBUGSNS("CO2 concentration: ", scd30_co2_ppm, " ppm");
}

void
loop()
{
  measurement();
#if ARDUINOJSON_VERSION_MAJOR >= 6
  JsonObject uploadjson = createjson_v6();
#else
  JsonObject& uploadjson = createjson_v5();
#endif
  DEBUGS("Uploading data");
  LOGSERVER_UPLOAD_STATUS uploadstatus =
    client.upload(uploadjson,       // JSON data
                  DATA_SERVER,      // server
                  DATA_SERVER_PORT, // port
#if ARDUINOJSON_VERSION_MAJOR >= 6
                  LOGSERVER_UPLOAD_FORMAT::MSGPACK, // format
#else
                  LOGSERVER_UPLOAD_FORMAT::JSON, // format
#endif
                  DATA_SERVER_SALT, // salt
                  4,                // salt size
                  3,                // connection attempts
                  20e3              // response timeout ms
    );
  switch (uploadstatus) {
    case (LOGSERVER_UPLOAD_STATUS::UPLOAD_SUCCESSFUL):
      DEBUGS("Upload was successful");
      break;
    default:
      DEBUGSN("Upload failed. Error code: ", (uint8_t)uploadstatus);
      break;
  }
  delay(5e3);
}
