/*

# Uploading data to a co2logserver via GSM using the LogserverClient library.

## Hardware

- Arduino (Uno or Mega)
- GSM-Shield v2
  - properly wired (jump Pin 2 to Pin 10 for Mega, see
    https://www.arduino.cc/en/Guide/ArduinoGSMShield for details)

## Libraries

- GSM
- LogserverClient
- ArduinoMD5
- ArduinoJson (both v5 and v6 should work)

## Server

- python3-co2logserver
  - https://gitlab.com/tue-umphy/co2mofetten/python3-co2logserver
  - settings according to **co2logserver settings** below
  - configured to accept a salt as given below

*/

#include <ArduinoJson.h>
#include <GSM.h>
#include <LogserverClient.h>

#define PINNUMBER ""
#define GPRS_APN "ENTER-YOUR-APN-HERE"
#define GPRS_LOGIN "ENTER-LOGIN-HERE"
#define GPRS_PASSWORD "ENTER-PASSWORD-HERE"
#define MAX_GPRS_CONNECT_ATTEMPTS 5
#define GPRS_CONNECT_ATTEMPT_DELAY 1000

// co2logserver settings
#define DATA_SERVER "CO2LOGSERVER-IP-OR-DOMAIN"
#define DATA_SERVER_PATH "/upload"
#define DATA_SERVER_PORT 8080
#define DATA_SERVER_SALT "ENTER-YOUR-SALT-HERE"

#define DEBUGS(x)                                                             \
  Serial.println(F(x));                                                       \
  Serial.flush();
#define DEBUGSN(x, y)                                                         \
  Serial.print(F(x));                                                         \
  Serial.println(y);                                                          \
  Serial.flush();
#define DEBUGSNS(x, y, z)                                                     \
  Serial.print(F(x));                                                         \
  Serial.print(y);                                                            \
  Serial.println(F(z));                                                       \
  Serial.flush();

bool connectedWithGSM = false;

unsigned int bme280_pressure_hPa;
unsigned short bme280_humidity_percent;
unsigned int scd30_co2_ppm;
float scd30_temperature_c;

LogserverClient<GSMClient> client;
GPRS gprs;
/* #define GSM_DEBUG */
#ifdef GSM_DEBUG
GSM gsmAccess(true);
#else
GSM gsmAccess;
#endif

// For some reason, checking the ARDUINOJSON_VERSION_MAJOR is not set on every
// compilation run that (PlatformIO?) is performed. This might have to do with
// some preprocessing that is performed on *.ino-files before CPP-compilation.
// This means we have to define two differently named functions here to prevent
// compiler errors...
#if ARDUINOJSON_VERSION_MAJOR >= 6
JsonObject
createjson_v6()
{
  const int uploadjsoncapacity = 4 * JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(4);
  StaticJsonDocument<uploadjsoncapacity> uploadjsondoc;
  JsonObject uploadjson = uploadjsondoc.to<JsonObject>();
  JsonArray humidity_array =
    uploadjson.createNestedArray("bme280_humidity_percent");
  humidity_array.add(bme280_humidity_percent);
  JsonArray pressure_array =
    uploadjson.createNestedArray("bme280_pressure_hPa");
  pressure_array.add(bme280_pressure_hPa);
  JsonArray temperature_array =
    uploadjson.createNestedArray("scd30_temperature_c");
  temperature_array.add(scd30_temperature_c);
  JsonArray co2_array = uploadjson.createNestedArray("scd30_co2_ppm");
  co2_array.add(scd30_co2_ppm);
  return uploadjson;
}
#else
JsonObject&
createjson_v5()
{
  const int uploadjsoncapacity = 4 * JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(4);
  StaticJsonBuffer<uploadjsoncapacity> uploadjsonbuffer;
  JsonObject& uploadjson = uploadjsonbuffer.createObject();
  JsonArray& humidity_array =
    uploadjson.createNestedArray("bme280_humidity_percent");
  humidity_array.add(bme280_humidity_percent);
  JsonArray& pressure_array =
    uploadjson.createNestedArray("bme280_pressure_hPa");
  pressure_array.add(bme280_pressure_hPa);
  JsonArray& temperature_array =
    uploadjson.createNestedArray("scd30_temperature_c");
  temperature_array.add(scd30_temperature_c);
  JsonArray& co2_array = uploadjson.createNestedArray("scd30_co2_ppm");
  co2_array.add(scd30_co2_ppm);
  return uploadjson;
}
#endif

void
setup()
{
  Serial.begin(115200);
  randomSeed(analogRead(0));
}

void
measurement()
{
  bme280_humidity_percent = random(0, 100);
  DEBUGSNS("         humidity: ", bme280_humidity_percent, " %");
  bme280_pressure_hPa = random(900, 1100);
  DEBUGSNS("         pressure: ", bme280_pressure_hPa, " hPa");
  scd30_temperature_c = (float)random(100, 400) / 10;
  DEBUGSNS("      temperature: ", scd30_temperature_c, " °C");
  scd30_co2_ppm = random(10000);
  DEBUGSNS("CO2 concentration: ", scd30_co2_ppm, " ppm");
}

boolean
loginToGSM()
{
  DEBUGS("GSM: starting connecting procedure");
  // connection state
  int gsmBeginState = 0;
  int gprsAttachState = 0;
  int gsmConnectAttempts = 0;
  while (not connectedWithGSM) { // until we are connected
    gsmConnectAttempts++;        // counts our retries
    if (gsmConnectAttempts >= MAX_GPRS_CONNECT_ATTEMPTS) {
      DEBUGS("GSM: giving up connection attempts");
      connectedWithGSM = false;
      break;
    }
    DEBUGSN("GSM: connection attempt nr. ", gsmConnectAttempts);
    // true is for the modem restart true is for synchron
    gsmBeginState = gsmAccess.begin((char*)PINNUMBER, true, true);
    DEBUGSN("GSM: begin state: ", gsmBeginState);
    gprsAttachState = gprs.attachGPRS(
      // the casting is necessary as the GSM library should use const char *
      (char*)GPRS_APN,
      (char*)GPRS_LOGIN,
      (char*)GPRS_PASSWORD,
      true // synchronous mode
    );
    DEBUGSN("GSM: attach state: ", gprsAttachState);
    if ((gsmBeginState == GSM_READY) & (gprsAttachState == GPRS_READY)) {
      DEBUGS("GSM: connection is ready");
      connectedWithGSM = true;
      break;
    } else {
      DEBUGS("GSM: could not connect");
      DEBUGS("GSM: waiting 1s until next connection attempt...");
      delay(GPRS_CONNECT_ATTEMPT_DELAY);
    }
  }
  return connectedWithGSM;
}

void
loop()
{
  measurement();
#if ARDUINOJSON_VERSION_MAJOR >= 6
  JsonObject uploadjson = createjson_v6();
#else
  JsonObject& uploadjson = createjson_v5();
#endif
  if (connectedWithGSM) {
    DEBUGS("Uploading data");
    LOGSERVER_UPLOAD_STATUS uploadstatus =
      client.upload(uploadjson,       // JSON data
                    DATA_SERVER,      // server
                    DATA_SERVER_PORT, // port
#if ARDUINOJSON_VERSION_MAJOR >= 6
                    LOGSERVER_UPLOAD_FORMAT::MSGPACK, // format
#else
                    LOGSERVER_UPLOAD_FORMAT::JSON, // format
#endif
                    DATA_SERVER_SALT, // salt
                    4,                // salt size
                    3,                // connection attempts
                    20e3              // response timeout ms
      );
    switch (uploadstatus) {
      case (LOGSERVER_UPLOAD_STATUS::UPLOAD_SUCCESSFUL):
        DEBUGS("Upload was successful");
        break;
      default:
        DEBUGSN("Upload failed. Error code: ", (uint8_t)uploadstatus);
        break;
    }
  } else {
    loginToGSM();
  }
  delay(5e3);
}
