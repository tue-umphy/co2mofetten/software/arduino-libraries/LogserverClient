#ifndef LSC_h
#define LSC_h

#include <Arduino.h>

#include <TimeLib.h>
#include <stdlib.h>
#include <string.h>

#include <ArduinoJson.h>
#if defined(ARDUINOJSON_VERSION_MAJOR) & defined(ARDUINOJSON_VERSION_MINOR)
#if ARDUINOJSON_VERSION_MAJOR < 5
#error "LogserverClient library requires ArduinoJson >= 5"
#endif
#else
#pragma message("LogserverClient library requires ArduinoJson >= 5")
#endif

#include <MD5.h>

#ifdef LSC_DEBUG
#define LSC_DEBUG_PREFIX "LogserverClient: "
#define DS(x)                                                                 \
  Serial.print(F(LSC_DEBUG_PREFIX));                                          \
  Serial.println(F(x));                                                       \
  Serial.flush();
#define DSN(x, y)                                                             \
  Serial.print(F(LSC_DEBUG_PREFIX));                                          \
  Serial.print(F(x));                                                         \
  Serial.println(y);                                                          \
  Serial.flush();
#define DSNSN(s1, n1, s2, n2)                                                 \
  Serial.print(F(LSC_DEBUG_PREFIX));                                          \
  Serial.print(F(s1));                                                        \
  Serial.print(n1);                                                           \
  Serial.print(F(s2));                                                        \
  Serial.println(n2);                                                         \
  Serial.flush();
#define DSNS(x, y, z)                                                         \
  Serial.print(F(LSC_DEBUG_PREFIX));                                          \
  Serial.print(F(x));                                                         \
  Serial.print(y);                                                            \
  Serial.println(F(z));                                                       \
  Serial.flush();
#define DSD(x, t)                                                             \
  Serial.print(F(LSC_DEBUG_PREFIX));                                          \
  Serial.print(F(x));                                                         \
  Serial.print(year(t));                                                      \
  Serial.print(F("-"));                                                       \
  Serial.print(month(t));                                                     \
  Serial.print(F("-"));                                                       \
  Serial.print(day(t));                                                       \
  Serial.print(F(" "));                                                       \
  Serial.print(hour(t));                                                      \
  Serial.print(F(":"));                                                       \
  Serial.print(minute(t));                                                    \
  Serial.print(F(":"));                                                       \
  Serial.print(second(t));                                                    \
  Serial.println();                                                           \
  Serial.flush();
#else
#define DS(x)
#define DSN(x, y)
#define DSNS(x, y, z)
#define DSD(x, t)
#define DSNSN(s1, n1, s2, n2)
#endif

enum class LOGSERVER_UPLOAD_FORMAT
{
  JSON
#if ARDUINOJSON_VERSION_MAJOR >= 6
  ,
  MSGPACK
#endif
};

enum class LOGSERVER_UPLOAD_STATUS
{
  UPLOAD_SUCCESSFUL,
  NO_CONNECTION,
  SEND_PAYLOAD_TIMEOUT,
  CANNOT_SEND_HEADERS,
  NO_RESPONSE,
  RESPONSE_TIMEOUT,
  RESPONSE_INCOMPLETE_HEADERS,
  HTTP_ERROR_CODE,
  NO_JSON_RESPONSE,
  SERVER_SAYS_NO_UPLOAD
};

enum class LOGSERVER_CLIENT_ACTION
{
  WHOLE_UPLOAD,
  CONNECT,
  SEND_PAYLOAD,
  WAIT_RESPONSE,
  PARSING_HEADERS,
  RECEIVE_RESPONSE,
  __count
};

template<typename T, size_t SIZE>
size_t LSCArraySize(T (&)[SIZE])
{
  return SIZE;
}

template<class CLIENT_TYPE>
class LogserverClient : public CLIENT_TYPE
{
public:
  using CLIENT_TYPE::CLIENT_TYPE;
  LOGSERVER_UPLOAD_STATUS upload(
#if ARDUINOJSON_VERSION_MAJOR >= 6
    const JsonObject uploadjson,
#else
    const JsonObject& uploadjson,
#endif
    const char* server,
    const unsigned int port = 80,
#if ARDUINOJSON_VERSION_MAJOR >= 6
    enum LOGSERVER_UPLOAD_FORMAT format = LOGSERVER_UPLOAD_FORMAT::MSGPACK,
#else
    enum LOGSERVER_UPLOAD_FORMAT format = LOGSERVER_UPLOAD_FORMAT::JSON,
#endif
    const char* salt = nullptr,
    const size_t salt_size = 0,
    const size_t connection_retries = 1,
    const unsigned long timeout_ms = 15e3,
    const bool set_time = false);

  static void salted_payload_md5_hash_hex(const char* payload,
                                          const size_t payload_size,
                                          const char* salt,
                                          const size_t salt_size,
                                          char* md5str);

  const bool parseHeaders(int16_t& status_code,
                          int16_t& content_length,
                          time_t& response_time,
                          const unsigned long timeout_ms);

  const bool try_to_connect(const char* server,
                            const size_t port,
                            const size_t attempts = 1,
                            const unsigned long timeout_ms = 0);

private:
  unsigned long _timer_start_ms[(size_t)LOGSERVER_CLIENT_ACTION::__count];
  unsigned long _timer_timeout_ms[(size_t)LOGSERVER_CLIENT_ACTION::__count];

  void _start_timer(LOGSERVER_CLIENT_ACTION action,
                    const unsigned long timeout_ms);
  void _stop_timer(LOGSERVER_CLIENT_ACTION action);
  const bool _timer_over(LOGSERVER_CLIENT_ACTION action);
  const bool _timer_active(LOGSERVER_CLIENT_ACTION action);
  const unsigned long _timer_remaining_ms(LOGSERVER_CLIENT_ACTION action);
  const unsigned long _timer_passed_ms(LOGSERVER_CLIENT_ACTION action);
};

template<class CLIENT_TYPE>
void
LogserverClient<CLIENT_TYPE>::_start_timer(
  LOGSERVER_CLIENT_ACTION action,
  const unsigned long timeout_ms // 0 = infinite
)
{
  DSNSN("Starting timer ", (size_t)action, " with [ms] ", timeout_ms);
  _timer_timeout_ms[(size_t)action] = timeout_ms;
  _timer_start_ms[(size_t)action] = millis();
}

template<class CLIENT_TYPE>
void
LogserverClient<CLIENT_TYPE>::_stop_timer(LOGSERVER_CLIENT_ACTION action)
{
  DS("Stopping timer");
  _timer_timeout_ms[(size_t)action] = 0;
}

template<class CLIENT_TYPE>
const bool
LogserverClient<CLIENT_TYPE>::_timer_active(LOGSERVER_CLIENT_ACTION action)
{
  return _timer_timeout_ms[(size_t)action];
}

template<class CLIENT_TYPE>
const bool
LogserverClient<CLIENT_TYPE>::_timer_over(LOGSERVER_CLIENT_ACTION action)
{
  if (this->_timer_active(action)) {
    return this->_timer_remaining_ms(action) >
           _timer_timeout_ms[(size_t)action];
  } else {
    return false;
  }
}

template<class CLIENT_TYPE>
const unsigned long
LogserverClient<CLIENT_TYPE>::_timer_remaining_ms(
  LOGSERVER_CLIENT_ACTION action)
{
  if (this->_timer_active(action)) {
    return this->_timer_timeout_ms[(size_t)action] -
           this->_timer_passed_ms(action);
  } else {
    return 0;
  }
}

template<class CLIENT_TYPE>
const unsigned long
LogserverClient<CLIENT_TYPE>::_timer_passed_ms(LOGSERVER_CLIENT_ACTION action)
{
  return millis() - _timer_start_ms[(size_t)action];
}

template<class CLIENT_TYPE>
const bool
LogserverClient<CLIENT_TYPE>::try_to_connect(
  const char* server,
  const size_t port,
  const size_t attempts,         // 0 = infinite
  const unsigned long timeout_ms // 0 = infinite
)
{
  size_t attempt_nr = 1;
  if (timeout_ms) {
    this->_start_timer(LOGSERVER_CLIENT_ACTION::CONNECT, timeout_ms);
  }
  while (true) {
    if (attempts) {
      DSNSN("Connection attempt nr. ", attempt_nr, " of ", attempts);
    } else {
      DSN("Connection attempt nr. ", attempt_nr);
    }
    if (this->connect(server, port)) {
      DSNSN("Now connected to server ", server, ":", port);
      return true;
    } else {
      DSNSN("Could not connect to server ", server, ":", port);
    }
    if (attempts) {
      if (attempt_nr >= attempts) {
        DSNS("Giving up connecting to server after ", attempt_nr, " attempts");
        return false;
      }
    }
    if (this->_timer_over(LOGSERVER_CLIENT_ACTION::CONNECT)) {
      DSNS("Giving up connecting to server after ",
           this->_timer_passed_ms(LOGSERVER_CLIENT_ACTION::CONNECT),
           " ms");
      return false;
    }
    attempt_nr++;
  }
}

template<class CLIENT_TYPE>
void
LogserverClient<CLIENT_TYPE>::salted_payload_md5_hash_hex(
  const char* payload,
  const size_t payload_size,
  const char* salt,
  const size_t salt_size,
  char* md5str)
{
  MD5_CTX context;
  unsigned char salted_md5hash[16] = { 0 };
  MD5::MD5Init(&context);
  MD5::MD5Update(&context, payload, payload_size);
  MD5::MD5Update(&context, salt, salt_size);
  MD5::MD5Final(salted_md5hash, &context);
  MD5::make_digest(salted_md5hash, 16, md5str);
}

template<class CLIENT_TYPE>
const bool
LogserverClient<CLIENT_TYPE>::parseHeaders(int16_t& status_code,
                                           int16_t& content_length,
                                           time_t& response_time,
                                           const unsigned long timeout_ms)
{
  // e.g. HTTP/1.0 200 OK
  const char status_key[] = "HTTP/";
  char status_val[20] = { 0 };
  // e.g. Date: Thu, 07 Mar 2019 12:44:49 GMT
  const char date_key[] = "Date:";
  char date_val[30] = { 0 };
  const char monthNames[] = "JanFebMarAprMayJunJulAugSepOctNovDec";
  // e.g. Content-Length: 76
  const char content_length_key[] = "Content-Length:";
  char content_length_val[5] = { 0 };
  // header keys
  const char* header_keys[] = { status_key, date_key, content_length_key };
  // header values
  char* header_values[] = { status_val, date_val, content_length_val };
  // matched positions in header keys
  int16_t header_keys_match[LSCArraySize(header_keys)];
  // initialize header_keys_match
  for (size_t i = 0; i < LSCArraySize(header_keys); i++) {
    header_keys_match[i] = -1;
  }
  // start timer
  if (timeout_ms)
    this->_start_timer(LOGSERVER_CLIENT_ACTION::PARSING_HEADERS, timeout_ms);
  // headers end
  const char headers_end[] = "\r\n\r\n";
  int8_t headers_end_match = -1;
  bool headers_end_found = false;
  while (not headers_end_found) {
    // stop if timeout is over
    if (this->_timer_over(LOGSERVER_CLIENT_ACTION::PARSING_HEADERS)) {
      DSNS("Giving up parsing headers after ",
           this->_timer_passed_ms(LOGSERVER_CLIENT_ACTION::PARSING_HEADERS),
           "ms")
      return false;
    }
    // stop if server closed connection
    if (!this->available() && !this->connected()) {
      DSNS("Server closed connection before end of headers '",
           headers_end,
           "' was found");
      return false;
    }
    while (this->available()) {
      char c = this->read();
      headers_end_match =
        (c == headers_end[headers_end_match + 1]) ? headers_end_match + 1 : -1;
      headers_end_found =
        (size_t)(headers_end_match + 1) >= strlen(headers_end);
      if (headers_end_found) {
        break;
      }
#ifdef LSC_DEBUG
      Serial.print(c);
#endif // #ifdef LSC_DEBUG
      // match all header keys
      for (size_t i = 0; i < LSCArraySize(header_keys); i++) {
        if ((size_t)(header_keys_match[i] + 1) >= strlen(header_keys[i])) {
          if (c == 0x0A or c == 0x0D) { // if we hit an EOL
            header_keys_match[i] = -1;  // stop filling this header value
          } else {                      // fill header value
            header_values[i][strlen(header_values[i])] = c;
          }
          break; // we can only have one header at a time, so stop checking
        } else {
          const char next_header_key_char =
            header_keys[i][header_keys_match[i] + 1];
          header_keys_match[i] = (tolower(c) == tolower(next_header_key_char))
                                   ? header_keys_match[i] + 1
                                   : -1;
          if ((size_t)(header_keys_match[i] + 1) >= strlen(header_keys[i]))
            break; // we can only have one header at a time, so stop checking
        }
      }
    }
  }
  // header is over
  for (size_t i = 0; i < LSCArraySize(header_keys); i++) {
    const char* key = header_keys[i];
    const char* val = header_values[i];
    DSNSN("Header '", key, "': ", val);
    if (not strcmp(key, content_length_key)) {
      content_length = atoi(val);
      DSN("Content-Length is ", content_length);
    } else if (not strcmp(key, status_key)) {
      char* pVersionEnd;
#ifdef LSC_DEBUG
      float httpVersion = 0.0;
      httpVersion = strtod(val, &pVersionEnd);
      DSN("HTTP protocol version is ", httpVersion);
#else
      strtod(val, &pVersionEnd);
#endif // #ifdef LSC_DEBUG
      status_code = atoi(pVersionEnd);
      DSN("HTTP status code is ", status_code);
    } else if (not strcmp(key, date_key)) {
      const char sep[] = " ,:";
      tmElements_t tm;
      char* pch;
      uint8_t i = 0;
      pch = strtok((char*)val, sep); // find first token
      while (pch != nullptr) {
        switch (i++) {
          case 1:
            tm.Day = atoi(pch);
            break;
          case 2: {
            // find month string in monthNames
            char* pchs = strstr(monthNames, pch);
            if (pchs)
              tm.Month = (pchs - monthNames) / 3 + 1;
            break;
          }
          case 3:
            tm.Year = CalendarYrToTm(atoi(pch));
            break;
          case 4:
            tm.Hour = atoi(pch);
            break;
          case 5:
            tm.Minute = atoi(pch);
            break;
          case 6:
            tm.Second = atoi(pch);
            break;
        }
        pch = strtok(nullptr, sep); // find next token
      }
      response_time = makeTime(tm); // parse time
      DSD("Response time: ", response_time);
    }
  }
  return true;
}

template<class CLIENT_TYPE>
LOGSERVER_UPLOAD_STATUS
LogserverClient<CLIENT_TYPE>::upload(
#if ARDUINOJSON_VERSION_MAJOR >= 6
  const JsonObject uploadjson,
#else
  const JsonObject& uploadjson,
#endif
  const char* server,
  const unsigned int port,
  enum LOGSERVER_UPLOAD_FORMAT format,
  const char* salt,
  const size_t salt_size,
  const size_t connection_retries,
  const unsigned long timeout_ms,
  const bool set_time)
{
  this->_start_timer(LOGSERVER_CLIENT_ACTION::WHOLE_UPLOAD, timeout_ms);
#ifdef LSC_DEBUG
  switch (format) {
#if ARDUINOJSON_VERSION_MAJOR >= 6
    case (LOGSERVER_UPLOAD_FORMAT::MSGPACK):
      DS("Going to upload MsgPack:");
      break;
#endif
    case (LOGSERVER_UPLOAD_FORMAT::JSON):
      DS("Going to upload JSON:");
      break;
  }
#if ARDUINOJSON_VERSION_MAJOR >= 6
  serializeJson(uploadjson, Serial);
  Serial.println();
#else
  uploadjson.printTo(Serial);
  Serial.println();
#endif
#endif
  this->setTimeout(timeout_ms); // does not do anything?
  DSNSN("Connecting to server ", server, ":", port);
  if (!this->try_to_connect(
        server,
        port,
        connection_retries,
        this->_timer_remaining_ms(LOGSERVER_CLIENT_ACTION::WHOLE_UPLOAD))) {
    return LOGSERVER_UPLOAD_STATUS::NO_CONNECTION;
  }
  DS("Sending headers");
  // Using HTTP/1.0 to prevent chunked transfer encoding
  // https://arduinojson.org/v5/faq/why-parsing-fails/
  this->println("POST /upload HTTP/1.0");
  this->print("Host: ");
  this->println(server);
  size_t payload_size = 0;
  switch (format) {
    case LOGSERVER_UPLOAD_FORMAT::JSON:
#if ARDUINOJSON_VERSION_MAJOR >= 6
      payload_size = measureJson(uploadjson);
#else
      payload_size = uploadjson.measureLength();
#endif
      this->println("Content-Type: application/json");
      break;
#if ARDUINOJSON_VERSION_MAJOR >= 6
    case LOGSERVER_UPLOAD_FORMAT::MSGPACK:
      payload_size = measureMsgPack(uploadjson);
      this->println("Content-Type: application/msgpack");
      break;
#endif
  }
  this->print("Content-Length: ");
  this->println(payload_size);
  DSN("Payload size is ", payload_size);
  // serializeJson and serializeMsgPack both serialize in a way that ensures
  // the last byte is a null terminator (0x00-byte). If the output size is
  // too small to hold the full serialization, the last byte will still be
  // the null terminator. Thus, we need to serialize to a buffer that can
  // just hold the null terminator as well
  char payload_with_null_terminator[payload_size + 1];
  switch (format) {
#if ARDUINOJSON_VERSION_MAJOR >= 6
    case LOGSERVER_UPLOAD_FORMAT::MSGPACK:
      serializeMsgPack(
        uploadjson, payload_with_null_terminator, payload_size + 1);
      break;
#endif
    case LOGSERVER_UPLOAD_FORMAT::JSON:
#if ARDUINOJSON_VERSION_MAJOR >= 6
      serializeJson(
        uploadjson, payload_with_null_terminator, payload_size + 1);
#else
      uploadjson.printTo(payload_with_null_terminator, payload_size + 1);
#endif
      break;
  }
#ifdef LSC_DEBUG
  DS("Payload is:");
  for (size_t i = 0; i < payload_size; i++) {
    Serial.print(payload_with_null_terminator[i]);
  }
  Serial.println();
#endif
  if (salt != nullptr) {
    DS("Calculating salted MD5 hash");
    char salted_md5hash_hex[16 + 1] = { 0 };
    this->salted_payload_md5_hash_hex(payload_with_null_terminator,
                                      payload_size,
                                      salt,
                                      salt_size,
                                      salted_md5hash_hex);
    DSNSN("Salted with '",
          salt,
          "', the MD5 payload hash is ",
          salted_md5hash_hex);
    this->print("Content-MD5-Salted: ");
    this->println(salted_md5hash_hex);
  }
  if (this->println() == 0) {
    DS("Failed to send request");
    this->stop();
    return LOGSERVER_UPLOAD_STATUS::CANNOT_SEND_HEADERS;
  }
  DS("Headers sent");
#ifdef LSC_DEBUG
  DS("Sending data");
  Serial.println(payload_with_null_terminator);
#endif
  this->_start_timer(
    LOGSERVER_CLIENT_ACTION::SEND_PAYLOAD,
    this->_timer_remaining_ms(LOGSERVER_CLIENT_ACTION::WHOLE_UPLOAD));
  for (size_t i = 0; i < payload_size; i++) {
    if (this->_timer_over(LOGSERVER_CLIENT_ACTION::SEND_PAYLOAD)) {
      DSNS("Giving up sending payload after ",
           this->_timer_passed_ms(LOGSERVER_CLIENT_ACTION::SEND_PAYLOAD),
           "ms");
      this->stop();
      return LOGSERVER_UPLOAD_STATUS::SEND_PAYLOAD_TIMEOUT;
    }
    this->print(payload_with_null_terminator[i]);
  }
  DS("Payload was sent");
  this->_start_timer(
    LOGSERVER_CLIENT_ACTION::WAIT_RESPONSE,
    this->_timer_remaining_ms(LOGSERVER_CLIENT_ACTION::WHOLE_UPLOAD));
  DS("Waiting for server response...");
  while (true) {
    if (this->_timer_over(LOGSERVER_CLIENT_ACTION::WAIT_RESPONSE)) {
      DSNS("Giving up waiting for server response after ",
           this->_timer_passed_ms(LOGSERVER_CLIENT_ACTION::WAIT_RESPONSE),
           "ms");
      this->stop();
      return LOGSERVER_UPLOAD_STATUS::NO_RESPONSE;
    }
    if (this->available()) {
      DS("Received something from the server");
      break;
    }
  }
  DS("Parsing headers");

  int16_t response_status_code = -1;
  int16_t response_content_length = -1;
  time_t response_time = 0;
  if (not this->parseHeaders(
        response_status_code,
        response_content_length,
        response_time,
        this->_timer_remaining_ms(LOGSERVER_CLIENT_ACTION::WHOLE_UPLOAD))) {
    DS("Couldn't parse headers.");
    this->stop();
    return LOGSERVER_UPLOAD_STATUS::RESPONSE_INCOMPLETE_HEADERS;
  }
  if (set_time) {
    if (response_time) {
      DSD("Setting time from response header to ", response_time);
    } else {
      DS("WARNING: No time in response header. Cannot set time.");
    }
  }
  if (response_status_code > 0 and
      not(200 <= response_status_code and response_status_code < 300)) {
    DSN("Server reports HTTP error status code ", response_status_code);
#ifdef LSC_DEBUG
    DS("Rest of server response:");
    while (this->available()) {
      Serial.print((char)this->read());
    }
    Serial.println();
#endif
    this->stop();
    return LOGSERVER_UPLOAD_STATUS::HTTP_ERROR_CODE;
  }
  DSN("Response Content-Length: ", response_content_length);
  const int responsejsoncapacity = JSON_OBJECT_SIZE(10) + 500;
#if ARDUINOJSON_VERSION_MAJOR >= 6
  StaticJsonDocument<responsejsoncapacity> responsejsondoc;
  DeserializationError error = deserializeJson(responsejsondoc, *this);
#else
  StaticJsonBuffer<responsejsoncapacity> responsejsonbuffer;
  JsonObject& responsejson = responsejsonbuffer.parseObject(*this);
  const bool error = !responsejson.success();
#endif
  if (error) {
#if ARDUINOJSON_VERSION_MAJOR >= 6
    DSN("Could not parse response as JSON:", error.c_str());
#else
    DS("Could not parse response as JSON");
#endif
#ifdef LSC_DEBUG
    DS("Rest of server response:");
    while (this->available()) {
      Serial.print((char)this->read());
    }
    Serial.println();
#endif
    this->stop();
    return LOGSERVER_UPLOAD_STATUS::NO_JSON_RESPONSE;
  }
#if ARDUINOJSON_VERSION_MAJOR >= 6
  JsonObject responsejson = responsejsondoc.as<JsonObject>();
#endif
#ifdef LSC_DEBUG
  DS("Parsed server response as JSON:");
#if ARDUINOJSON_VERSION_MAJOR >= 6
  serializeJson(responsejson, Serial);
  Serial.println();
#else
  responsejson.printTo(Serial);
  Serial.println();
#endif
#endif
  const size_t saved_in_db = responsejson["saved-in-db"];
  DSNS("Server reports that ", saved_in_db, " datasets were saved");
  DS("Stopping client");
  this->stop();
  DSNS("The upload took ",
       this->_timer_passed_ms(LOGSERVER_CLIENT_ACTION::WHOLE_UPLOAD),
       " ms");
  if (saved_in_db > 0) {
    return LOGSERVER_UPLOAD_STATUS::UPLOAD_SUCCESSFUL;
  } else {
    return LOGSERVER_UPLOAD_STATUS::SERVER_SAYS_NO_UPLOAD;
  }
}

#endif
